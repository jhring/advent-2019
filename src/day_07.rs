use std::cmp::max;
use std::fs::read_to_string;

use itertools::Itertools;

fn int_to_vec(x: u32) -> Vec<u32> {
    let mut digits = vec![];
    let mut x = x;
    while x > 9 {
        digits.push(x % 10);
        x /= 10;
    }
    digits.push(x);
    digits.reverse();
    digits
}

fn parse_instruction(ins: u32) -> (u32, Vec<u32>) {
    let mut digits = int_to_vec(ins);
    digits.reverse();

    let opcode = if digits.len() > 1 {
        digits[0] + 10 * digits[1]
    } else {
        digits[0]
    };

    let num_params = match opcode {
        1 => 2,
        2 => 2,
        3 => 0,
        4 => 1,
        5 => 2,
        6 => 2,
        7 => 2,
        8 => 2,
        99 => 0,
        _ => unreachable!(),
    };
    let mut param_modes = vec![0; num_params];
    for i in 2..digits.len() {
        param_modes[i - 2] = digits[i];
    }
    (opcode, param_modes)
}

struct Thruster {
    data: Vec<i32>,
    ins_pointer: usize,
    result: i32,
    last_opcode: u32,
}

impl Thruster {
    fn new(data: &[i32]) -> Self {
        Thruster {
            data: data.to_owned(),
            ins_pointer: 0,
            result: 0,
            last_opcode: 0,
        }
    }

    fn intcode_computer(&mut self, inputs: &[i32]) {
        let mut input_idx = 0;
        while self.ins_pointer <= self.data.len() {
            let (opcode, param_modes) = parse_instruction(self.data[self.ins_pointer] as u32);

            let mut params = vec![0; param_modes.len()];
            for param_idx in 0..param_modes.len() {
                let mut param_val = self.data[self.ins_pointer + 1 + param_idx];
                if param_modes[param_idx] == 0 {
                    param_val = self.data[param_val as usize]
                }
                params[param_idx] = param_val;
            }

            self.last_opcode = opcode;
            match opcode {
                1 => {
                    let idx = self.data[self.ins_pointer + 3] as usize;
                    self.data[idx] = params[0] + params[1];
                    self.ins_pointer += 4;
                }
                2 => {
                    let idx = self.data[self.ins_pointer + 3] as usize;
                    self.data[idx] = params[0] * params[1];
                    self.ins_pointer += 4;
                }
                3 => {
                    if input_idx < inputs.len() {
                        let idx = self.data[self.ins_pointer + 1] as usize;
                        self.data[idx] = inputs[input_idx];
                        input_idx += 1;
                        self.ins_pointer += 2;
                    } else {
                        break;
                    }
                }
                4 => {
                    self.ins_pointer += 2;
                    self.result = params[0];
                    break;
                }
                5 => {
                    if params[0] != 0 {
                        self.ins_pointer = params[1] as usize;
                    } else {
                        self.ins_pointer += 3;
                    }
                }
                6 => {
                    if params[0] == 0 {
                        self.ins_pointer = params[1] as usize;
                    } else {
                        self.ins_pointer += 3
                    }
                }
                7 => {
                    let idx = self.data[self.ins_pointer + 3] as usize;
                    if params[0] < params[1] {
                        self.data[idx] = 1
                    } else {
                        self.data[idx] = 0
                    }
                    self.ins_pointer += 4;
                }
                8 => {
                    let idx = self.data[self.ins_pointer + 3] as usize;
                    if params[0] == params[1] {
                        self.data[idx] = 1;
                    } else {
                        self.data[idx] = 0
                    }
                    self.ins_pointer += 4;
                }
                99 => break,
                _ => unreachable!(),
            }
        }
    }
}

impl Clone for Thruster {
    fn clone(&self) -> Thruster {
        Thruster {
            data: self.data.to_owned(),
            ins_pointer: self.ins_pointer,
            result: self.result,
            last_opcode: self.last_opcode,
        }
    }
}

fn part2(data_org: &[i32]) -> i32 {
    let mut max_val = i32::min_value();
    for phases in (5..10 as i32).permutations(5 as usize) {
        let mut thrusters = vec![Thruster::new(&data_org.to_owned()); 5];

        for (thruster_idx, phase) in phases.iter().enumerate() {
            thrusters[thruster_idx].intcode_computer(&[*phase]);
        }

        let mut thruster_idx = 4;
        while thrusters[thruster_idx].last_opcode != 99 {
            let next = (thruster_idx + 1) % 5;
            let input = thrusters[thruster_idx].result;
            thrusters[next].intcode_computer(&[input]);
            thruster_idx = next;
        }
        max_val = max(thrusters[4].result, max_val);
    }
    max_val
}

fn test_amps(data: &[i32], num_amps: u32) -> i32 {
    let mut max_val = i32::min_value();
    for phases in (0..num_amps as i32).permutations(num_amps as usize) {
        let mut sig = 0;
        for phase in phases {
            let mut thruster = Thruster::new(&data.to_owned());
            let inputs = [phase, sig];
            thruster.intcode_computer(&inputs);
            sig = thruster.result;
        }
        max_val = max(sig, max_val);
    }
    max_val
}

pub fn main() {
    let data: Vec<i32> = read_to_string("data/day7.txt")
        .unwrap()
        .trim()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();
    let p1 = test_amps(&data, 5);
    let p2 = part2(&data);
    println!("Part 1: {}", p1);
    println!("Part 2: {}", p2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn day7_test1() {
        let data = vec![
            3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0,
        ];
        let res = test_amps(&data, 5);
        assert_eq!(res, 43210);
    }

    #[test]
    pub fn day7_test2() {
        let data = vec![
            3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23,
            99, 0, 0,
        ];
        let res = test_amps(&data, 5);
        assert_eq!(res, 54321);
    }

    #[test]
    pub fn day7_test3() {
        let data = vec![
            3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1,
            33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0,
        ];
        let res = test_amps(&data, 5);
        assert_eq!(res, 65210);
    }

    #[test]
    pub fn day7_test4() {
        let data = vec![
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1,
            28, 1005, 28, 6, 99, 0, 0, 5,
        ];
        assert_eq!(part2(&data), 139_629_729);
    }

    #[test]
    pub fn day7_test5() {
        let data = vec![
            3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
            -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
            53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10,
        ];
        assert_eq!(part2(&data), 18216);
    }
}
