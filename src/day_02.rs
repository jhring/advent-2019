use std::fs::read_to_string;

fn intcode_compute(data: &mut [u32], noun: u32, verb: u32) {
    data[1] = noun;
    data[2] = verb;
    let mut end = 4;
    while end <= data.len() {
        if let [opcode, arg1_idx, arg2_idx, pos] = data[end - 4..end] {
            let arg1 = data[arg1_idx as usize];
            let arg2 = data[arg2_idx as usize];
            let pos = pos as usize;
            match opcode {
                1 => data[pos] = arg1 + arg2,
                2 => data[pos] = arg1 * arg2,
                99 => break,
                _ => println!("Fatal Error"),
            }
        }
        end += 4;
    }
}

pub fn main() {
    let data: Vec<u32> = read_to_string("data/day2.txt")
        .unwrap()
        .trim()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();
    let mut data_tmp = data.clone();
    intcode_compute(&mut data_tmp, 12, 2);
    let p1 = data_tmp[0];
    let mut p2 = 0;
    let target = 19_690_720;
    'noun: for noun in 0..100 {
        for verb in 0..100 {
            data_tmp = data.clone();
            intcode_compute(&mut data_tmp, noun, verb);
            if data_tmp[0] == target {
                p2 = 100 * noun + verb;
                break 'noun;
            }
        }
    }
    println!("Gravity assist exit (Part 1): {}", p1);
    println!("Gravity assist parameter (Part 2): {}", p2);
}

#[cfg(test)]
pub mod tests {

    use super::*;

    #[test]
    pub fn day2_test1() {
        let mut data = [1, 0, 0, 0, 99];
        intcode_compute(&mut data, 0, 0);
        assert_eq!(data, [2, 0, 0, 0, 99]);
    }

    #[test]
    pub fn day2_test2() {
        let mut data = [2, 3, 0, 3, 99];
        intcode_compute(&mut data, 3, 0);
        assert_eq!(data, [2, 3, 0, 6, 99]);
    }

    #[test]
    pub fn day2_test3() {
        let mut data = [2, 4, 4, 5, 99, 0];
        intcode_compute(&mut data, 4, 4);
        assert_eq!(data, [2, 4, 4, 5, 99, 9801]);
    }

    #[test]
    pub fn day2_test4() {
        let mut data = [1, 1, 1, 4, 99, 5, 6, 0, 99];
        intcode_compute(&mut data, 1, 1);
        assert_eq!(data, [30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }
}
