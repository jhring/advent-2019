pub fn int_to_vec(x: u32) -> Vec<u32> {
    let mut digits = vec![];
    let mut x = x;
    while x > 9 {
        digits.push(x % 10);
        x /= 10;
    }
    digits.push(x);
    digits.reverse();
    digits
}

pub fn main() {
    let start = 347_312;
    let end = 805_915;
    let mut count1 = 0;
    let mut count2 = 0;
    for val in start..end + 1 {
        let digits = int_to_vec(val);
        let mut sorted_digits = digits.clone();
        sorted_digits.sort();
        if digits == sorted_digits {
            let mut counts = [0; 10];
            for d in digits {
                counts[d as usize] += 1;
            }
            if counts.iter().max().unwrap() > &1 {
                count1 += 1;
            }
            if counts.contains(&2) {
                count2 += 1;
            }
        }
    }
    println!("Possible passwords (Part 1): {:#?}", count1);
    println!("Possible passwords (Part 2): {:#?}", count2);
}
