use std::fs::read_to_string;

fn fuel_required_rec(mass: u32) -> u32 {
    // max value where mass / 3 > 2
    if mass > 8 {
        let fuel = mass / 3 - 2;
        fuel + fuel_required_rec(fuel)
    } else {
        0
    }
}

fn fuel_required(mass: u32) -> u32 {
    if mass > 8 {
        mass / 3 - 2
    } else {
        0
    }
}

pub fn main() {
    let mut data: Vec<u32> = vec![];
    for line in read_to_string("data/day1.txt").unwrap().lines() {
        data.push(line.parse().unwrap())
    }
    let fuel1: u32 = data.iter().map(|x| fuel_required(*x)).sum();
    let fuel2: u32 = data.iter().map(|x| fuel_required_rec(*x)).sum();
    println!("Fuel required (Part 1): {}", fuel1);
    println!("Rec fuel required (Part 2): {}", fuel2);
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn day1_test1() {
        assert_eq!(2, fuel_required(12));
    }

    #[test]
    fn day1_test2() {
        assert_eq!(2, fuel_required(14));
    }

    #[test]
    fn day1_test3() {
        assert_eq!(654, fuel_required(1969));
    }

    #[test]
    fn day1_test4() {
        assert_eq!(33583, fuel_required(100_756));
    }

    #[test]
    fn day1_test5() {
        assert_eq!(2, fuel_required_rec(14));
    }

    #[test]
    fn day1_test6() {
        assert_eq!(966, fuel_required_rec(1969));
    }

    #[test]
    fn day1_test7() {
        assert_eq!(50346, fuel_required_rec(100_756));
    }
}
