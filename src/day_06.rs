use std::collections::{HashMap, VecDeque};
use std::fs::read_to_string;

fn indirect_orbits(orbit_map: &HashMap<String, String>, key: &str) -> u32 {
    if orbit_map.contains_key(key) {
        1 + indirect_orbits(orbit_map, orbit_map.get(key).unwrap())
    } else {
        0
    }
}

fn bfs(adj_list: &HashMap<&String, Vec<&String>>, source: &String, target: &String) -> u32 {
    let mut queue = VecDeque::new();
    let mut visited = Vec::with_capacity(adj_list.len());
    let mut dist = HashMap::new();

    visited.push(source);
    dist.entry(source).or_insert(0);
    queue.push_back(source);

    'outside: while !queue.is_empty() {
        let u = queue.pop_front().unwrap();
        for neighbor in adj_list.get(u).unwrap() {
            if !visited.contains(&neighbor) {
                visited.push(neighbor);
                *dist.entry(neighbor).or_insert(0) += dist[u] + 1;
                queue.push_back(neighbor);

                if *neighbor == target {
                    break 'outside;
                }
            }
        }
    }
    dist[target]
}

pub fn main() {
    let mut orbit_map = HashMap::new();
    for line in read_to_string("data/day6.txt").unwrap().lines() {
        let x: Vec<&str> = line.split(')').collect();
        orbit_map.insert(x[1].to_string(), x[0].to_string());
    }
    let mut count = 0;
    let mut adj_list = HashMap::new();
    for (orbiter, orbited) in &orbit_map {
        count += 1 + indirect_orbits(&orbit_map, orbited);
        adj_list
            .entry(orbiter)
            .or_insert_with(Vec::new)
            .push(orbited);
        adj_list
            .entry(orbited)
            .or_insert_with(Vec::new)
            .push(orbiter);
    }
    println!("Total indirect orbits (Part 1): {}", count);
    println!(
        "Orbital transfers required (Part 2): {:#?}",
        bfs(
            &adj_list,
            orbit_map.get("YOU").unwrap(),
            orbit_map.get("SAN").unwrap()
        )
    );
}
