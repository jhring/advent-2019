use std::collections::HashSet;
use std::fs::read_to_string;
use std::iter::FromIterator;

fn to_coords(wire: &[&str]) -> Vec<(i32, i32)> {
    let mut coords = vec![(0, 0)];
    for elm in wire {
        let mut value: i32 = elm[1..].parse().unwrap();
        let direction = elm.chars().next().unwrap();
        while value > 0 {
            let (mut x, mut y) = coords.last().copied().unwrap();
            match direction {
                'L' => x -= 1,
                'R' => x += 1,
                'U' => y += 1,
                'D' => y -= 1,
                _ => println!("Fatal Error"),
            }
            coords.push((x, y));
            value -= 1;
        }
    }
    coords
}

fn manhattan_distance(x1: i32, y1: i32, x2: i32, y2: i32) -> i32 {
    (x1 - x2).abs() + (y1 - y2).abs()
}

pub fn main() {
    let data: String = read_to_string("data/day3.txt").unwrap();
    let data: Vec<&str> = data.split('\n').collect();
    let mut wires: Vec<Vec<&str>> = vec![];
    for line in data.clone() {
        wires.push(line.split(',').collect())
    }
    let c1 = to_coords(&wires[0]);
    let c2 = to_coords(&wires[1]);
    let h1: HashSet<(i32, i32)> = HashSet::from_iter(c1.clone());
    let h2: HashSet<(i32, i32)> = HashSet::from_iter(c2.clone());
    let mut inter: HashSet<_> = h1.intersection(&h2).collect();
    inter.remove(&(0, 0));
    let minval = inter
        .iter()
        .map(|(x, y)| manhattan_distance(0, 0, *x, *y))
        .min()
        .unwrap();
    println!("Closest intersection distance (Part 1): {:#?}", minval);
    let min_step = inter
        .iter()
        .map(|x| {
            c1.iter().position(|y| y == *x).unwrap() + c2.iter().position(|y| y == *x).unwrap()
        })
        .min()
        .unwrap();
    println!("Shortest wire crossing (Part 2): {:#?}", min_step);
}
