use std::fs::read_to_string;

pub fn int_to_vec(x: u32) -> Vec<u32> {
    let mut digits = vec![];
    let mut x = x;
    while x > 9 {
        digits.push(x % 10);
        x /= 10;
    }
    digits.push(x);
    digits.reverse();
    digits
}

fn parse_instruction(ins: u32) -> (u32, Vec<u32>) {
    let mut digits = int_to_vec(ins);
    digits.reverse();

    let opcode = if digits.len() > 1 {
        digits[0] + 10 * digits[1]
    } else {
        digits[0]
    };

    let num_params = match opcode {
        1 => 2,
        2 => 2,
        3 => 0,
        4 => 1,
        5 => 2,
        6 => 2,
        7 => 2,
        8 => 2,
        99 => 0,
        _ => unreachable!(),
    };
    let mut param_modes = vec![0; num_params];
    for i in 2..digits.len() {
        param_modes[i - 2] = digits[i];
    }
    (opcode, param_modes)
}

fn intcode_computer(data: &mut [i32]) {
    let mut idx = 0;
    while idx <= data.len() {
        let (opcode, param_modes) = parse_instruction(data[idx] as u32);
        // get args
        let mut params = vec![0; param_modes.len()];
        for param_idx in 0..param_modes.len() {
            let mut param_val = data[idx + 1 + param_idx];
            if param_modes[param_idx] == 0 {
                param_val = data[param_val as usize]
            }
            params[param_idx] = param_val;
        }
        match opcode {
            1 => {
                data[data[idx + 3] as usize] = params[0] + params[1];
                idx += 4;
            }
            2 => {
                data[data[idx + 3] as usize] = params[0] * params[1];
                idx += 4;
            }
            3 => {
                // user interaction disabled for testing
                // let mut input = String::new();
                // println!("Please enter a value: ");
                // std::io::stdin().read_line(&mut input);
                // let value: i32 = input.trim().parse().unwrap();
                let value = 5;
                data[data[idx + 1] as usize] = value;
                idx += 2
            }
            4 => {
                println!("Your value is {}", params[0]);
                idx += 2;
            }
            5 => {
                if params[0] != 0 {
                    idx = params[1] as usize;
                } else {
                    idx += 3;
                }
            }
            6 => {
                if params[0] == 0 {
                    idx = params[1] as usize;
                } else {
                    idx += 3
                }
            }
            7 => {
                if params[0] < params[1] {
                    data[data[idx + 3] as usize] = 1
                } else {
                    data[data[idx + 3] as usize] = 0
                }
                idx += 4;
            }
            8 => {
                if params[0] == params[1] {
                    data[data[idx + 3] as usize] = 1;
                } else {
                    data[data[idx + 3] as usize] = 0
                }
                idx += 4;
            }
            99 => break,
            _ => unreachable!(),
        }
    }
}

pub fn main() {
    let mut data: Vec<i32> = read_to_string("data/day5.txt")
        .unwrap()
        .trim()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();
    intcode_computer(&mut data);
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    pub fn day2_test1() {
        let mut data = [1, 0, 0, 0, 99];
        intcode_computer(&mut data);
        assert_eq!(data, [2, 0, 0, 0, 99]);
    }

    #[test]
    pub fn day2_test2() {
        let mut data = [2, 3, 0, 3, 99];
        intcode_computer(&mut data);
        assert_eq!(data, [2, 3, 0, 6, 99]);
    }

    #[test]
    pub fn day2_test3() {
        let mut data = [2, 4, 4, 5, 99, 0];
        intcode_computer(&mut data);
        assert_eq!(data, [2, 4, 4, 5, 99, 9801]);
    }

    #[test]
    pub fn day2_test4() {
        let mut data = [1, 1, 1, 4, 99, 5, 6, 0, 99];
        intcode_computer(&mut data);
        assert_eq!(data, [30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }

    #[test]
    fn day5_test1() {
        let mut data = [1002, 4, 3, 4, 33];
        intcode_computer(&mut data);
        assert_eq!(data, [1002, 4, 3, 4, 99])
    }
}

/*
Debug functions that require user input

fn equals_8_pos() {
    let mut data = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
    intcode_compute(&mut data);
}

fn less_than_8_pos() {
    let mut data = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
    intcode_compute(&mut data);
}

fn equals_8_imm() {
    let mut data = [3, 3, 1108, -1, 8, 3, 4, 3, 99];
    intcode_compute(&mut data);
}

fn less_than_8_imm() {
    let mut data = [3, 3, 1107, -1, 8, 3, 4, 3, 99];
    intcode_compute(&mut data);
}

fn is_zero_pos() {
    let mut data = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
    intcode_compute(&mut data);
}
*/
