use std::fs::read_to_string;

fn int_to_vec(x: usize) -> Vec<usize> {
    let mut digits = vec![];
    let mut x = x;
    while x > 9 {
        digits.push(x % 10);
        x /= 10;
    }
    digits.push(x);
    digits.reverse();
    digits
}

fn parse_instruction(ins: usize) -> (usize, Vec<usize>) {
    let mut digits = int_to_vec(ins);
    digits.reverse();

    let opcode = if digits.len() > 1 {
        digits[0] + 10 * digits[1]
    } else {
        digits[0]
    };

    let num_params = match opcode {
        1 => 3,
        2 => 3,
        3 => 1,
        4 => 1,
        5 => 2,
        6 => 2,
        7 => 3,
        8 => 3,
        9 => 1,
        99 => 0,
        _ => unreachable!(),
    };
    let mut param_modes = vec![0; num_params];
    for i in 2..digits.len() {
        param_modes[i - 2] = digits[i];
    }
    (opcode, param_modes)
}

fn intcode_computer(data: &mut Vec<isize>, inputs: &[isize]) -> Vec<isize>{
    let mut ins_pointer = 0;
    let mut input_idx = 0;
    let mut relative_base = 0;
    let mut out = vec![];

    while ins_pointer <= data.len() {
        let (opcode, param_modes) = parse_instruction(data[ins_pointer] as usize);

        let mut params = vec![0; param_modes.len()];
        for (idx, mode) in param_modes.iter().enumerate() {
            let pointer = ins_pointer + 1 + idx;
            params[idx] = if *mode == 0 {
                (data[pointer]) as usize
            } else if *mode == 1 {
                pointer
            } else {
                (data[pointer] + relative_base) as usize
            };
        }

        match params.iter().max() {
            Some(max) => {
                if *max >= data.len() {
                    data.resize_with(*max + 1, Default::default);
                }
            },
            None => (),
        }

        match opcode {
            1 => {
                data[params[2]] = data[params[0]] + data[params[1]];
                ins_pointer += 4;
            }
            2 => {
                data[params[2]] = data[params[0]] * data[params[1]];
                ins_pointer += 4;
            }
            3 => {
                data[params[0]] = inputs[input_idx];
                input_idx += 1;
                ins_pointer += 2;
            }
            4 => {
                out.push(data[params[0]]);
                ins_pointer += 2;
            }
            5 => {
                if data[params[0]] != 0 {
                    ins_pointer = data[params[1]] as usize;
                } else {
                    ins_pointer += 3;
                }
            }
            6 => {
                if data[params[0]] == 0 {
                    ins_pointer = data[params[1]] as usize;
                } else {
                    ins_pointer += 3
                }
            }
            7 => {
                if data[params[0]] < data[params[1]] {
                    data[params[2]] = 1
                } else {
                    data[params[2]] = 0
                }
                ins_pointer += 4;
            }
            8 => {
                if data[params[0]] == data[params[1]] {
                    data[params[2]] = 1;
                } else {
                    data[params[2]] = 0;
                }
                ins_pointer += 4;
            }
            9 => {
                relative_base += data[params[0]];
                ins_pointer += 2
            }
            99 => break,
            _ => unreachable!(),
        }
    }
    out
}

pub fn main() {
    let data: Vec<isize> = read_to_string("data/day9.txt")
        .unwrap()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();

    println!("Part 1: {}", intcode_computer(&mut data.clone(), &[1])[0]);
    println!("Part 2: {}", intcode_computer(&mut data.clone(), &[2])[0]);
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn day9_test1() {
        let data = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
        assert_eq!(intcode_computer(&mut data.clone(), &[]), data);
    }

    #[test]
    fn day9_test2() {
        let mut data = vec![1102,34915192,34915192,7,4,7,99,0];
        assert_eq!(intcode_computer(&mut data, &[]), [1219070632396864]);
    }

    #[test]
    fn day9_test3() {
        let mut data = vec![104,1125899906842624,99];
        assert_eq!(intcode_computer(&mut data, &[]), [1125899906842624]);
    }

    #[test]
    fn day_5_solution() {
        let data: Vec<isize> = read_to_string("data/day5.txt")
            .unwrap()
            .trim()
            .split(',')
            .map(|s| s.parse().unwrap())
            .collect();
        let out = intcode_computer(&mut data.clone(), &[1]);
        assert_eq!(out, [0, 0, 0, 0, 0, 0, 0, 0, 0, 2845163]);
        let out = intcode_computer(&mut data.clone(), &[5]);
        assert_eq!(out, [9436229]);
    }
}