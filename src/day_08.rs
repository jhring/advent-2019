use std::fs::read_to_string;

use ndarray::prelude::*;
use ndarray::Array;


fn verify_transmission(data: &Array<u32, Ix3>) -> usize {
    let mut layer_idx = 0usize;
    let mut min_zeros = data.slice(s![0, .., ..]).iter().filter(|&n| *n == 0).count();

    for (idx, i) in data.outer_iter().enumerate() {
        let num_zeros = i.iter().filter(|&n| *n == 0).count();
        if num_zeros <= min_zeros {
            min_zeros = num_zeros;
            layer_idx = idx;
        }
    }

    let d1 = data.slice(s![layer_idx, .., ..]).iter().filter(|&n| *n == 1).count();
    let d2 = data.slice(s![layer_idx, .., ..]).iter().filter(|&n| *n == 2).count();
    d1 * d2
}


fn decode_image(data: &Array<u32, Ix3>) {
    for i in 0..6 {
        for j in 0..25 {
            let view = data.slice(s![.., i, j]);
            for elm in view {
                if *elm != 2 {
                    if *elm == 0 {
                        print!(" ");
                    } else {
                        print!("*");
                    }
                    break
                }
            }
        }
        print!("\n");
    }
}


pub fn main() {
    let data: Vec<u32> = read_to_string("data/day8.txt")
        .unwrap()
        .chars()
        .map(|s| s.to_digit(10).unwrap())
        .collect();
    let data = Array::<u32, Ix3>::from_shape_vec((100, 6, 25), data).unwrap();
    println!("Part 1: {}", verify_transmission(&data));
    println!("Part 2:");
    decode_image(&data);
}